#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sem título.py
#  
#  Copyright 2013 Alessandro Bandeira Duarte <alessandro@logica>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
###################################
#Achar raiz para 2x^2-4x=0
#####################################
from math import sqrt
a=input("Entre com um valor para a: ")
b=input("Entre com um valor para b: ")
c=input("Entre com um valor para c: ")
delta = (b**2)-(4*a*c)
if delta < 0:
	print("Não existe raiz")
else:
	x1=(-b+sqrt(delta))/(2.0*a)
	x2=(-b - sqrt(delta))/(2.0*a)
	print(x1)
	print(x2)

